# Generated by Django 3.0.5 on 2020-04-28 22:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emailTemplates', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Permissions',
            new_name='EmailTemplate',
        ),
    ]
