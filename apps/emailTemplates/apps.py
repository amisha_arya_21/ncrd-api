from django.apps import AppConfig


class EmailtemplatesConfig(AppConfig):
    name = 'apps.emailTemplates'
