from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers
from apps.user.models import User
from .models import UserReport
from apps.answers.serializers import AnswerSerializer
from apps.user.serializers import UserSerializer
import logging

logger = logging.getLogger(__name__)

class UserReportSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format="%d %b %Y")

    class Meta:
        model = UserReport
        fields = ('id','created_at')
        read_only_fields = ('id','created_at')


class UserReportCreateSerializer(serializers.Serializer):
    patient_id = serializers.IntegerField(min_value=1)
    added_by_user_type = serializers.CharField(max_length=20)
    added_by_user_id = serializers.IntegerField()

    def create(self, validated_data):
        return UserReport.objects.create(**validated_data)
