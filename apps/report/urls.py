from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import (
    ListReport, CreateReport, ReportUpdate, ReportDelete
)
from django.urls import path

app_name="report"
urlpatterns = [
    path('report/update/<int:id>', ReportUpdate.as_view(), name="update-report"),
    path('report/delete/<int:id>', ReportDelete.as_view(), name="delete-report"),
    path('report/create/<int:id>', CreateReport.as_view(), name="create-report"),
    path('report/<int:id>/', ListReport.as_view(), name="list-report"),
]
