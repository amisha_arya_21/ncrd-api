from django.db import models

class UserReport(models.Model):
    id = models.BigAutoField(primary_key=True)
    patient_id = models.BigIntegerField()
    added_by_user_type = models.CharField(max_length=20)
    added_by_user_id = models.BigIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'patient_report'

# Create your models here.
# class UserReport(models.Model):
#     id = models.BigAutoField(primary_key=True)
#     patient_id = models.BigIntegerField()
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#
#     class Meta:
#         managed = False
#         db_table = 'patient_report'
