from rest_framework import status, mixins, permissions, generics
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from apps.common.renderers import ConduitJSONRenderer
from .models import UserReport
from apps.user.models import User
from apps.patient.models import Patient
from apps.answers.models import UserReportAnswers
from apps.prescription.models import Prescriptions
from .renderers import ReportJSONRenderer
from apps.answers.serializers import (
    CreateAnswerSerializer, AnswerSerializer
)
from apps.user.serializers import UserSerializer
from .serializers import (
    UserReportSerializer, UserReportCreateSerializer
)
import json
from rest_framework.pagination import PageNumberPagination

class ListReport(generics.ListAPIView):
    serializer_class = UserReportSerializer
    permission_classes = (IsAuthenticated,)
    renderer_classes = (ReportJSONRenderer,)

    def get_queryset(self,patient_id):
        return UserReport.objects.filter(patient_id=patient_id).order_by('-created_at')

    def list(self, request, id):
        patient_id = id
        try:
            user = Patient.objects.get(id=patient_id)
        except Patient.DoesNotExist:
            raise ValidationError({'user': ['No patient found with this id.']})
        # queryset = self.get_queryset(patient_id)
        # serializer = self.serializer_class(queryset,many=True)
        # return Response({'data':serializer.data},
        #                 status=status.HTTP_200_OK)
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset(patient_id))

        serializer = self.serializer_class(
        page,
        context=serializer_context,
        many=True
        )
        return self.get_paginated_response(serializer.data)

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CreateReport(generics.CreateAPIView):
    serializer_class = UserReportCreateSerializer
    permission_classes = (IsAuthenticated,)
    renderer_classes = (ConduitJSONRenderer,)

    def create(self, request, id, *args, **kwargs):
        patient_id = id
        try:
            user = Patient.objects.get(id=patient_id)
        except Patient.DoesNotExist:
            raise ValidationError({'user': ['No patient found with this id.']})
        report_data = {}
        report_data['patient_id'] = patient_id
        report_data['added_by_user_type'] = "doctor"
        report_data["added_by_user_id"] = request.user.id
        report_serializer = self.get_serializer(data=report_data)
        report_serializer.is_valid(raise_exception=True)
        report = report_serializer.save()


        report_id = report.id
        answer_data = request.data['answers']

        for answer in answer_data:
            answer['report_id'] = report_id
            answer['patient_id'] = patient_id
            answers_serializer = CreateAnswerSerializer(data=answer)
            answers_serializer.is_valid(raise_exception=True)
            answers = answers_serializer.save()

        Prescriptions.objects.update_or_create(
            doctor_id=request.user.id,
            patient_id=patient_id,
            report_id=report_id,
            defaults={
            "prescription": request.data['patient_prescription']['prescription']
        })
        report = UserReportSerializer(report)
        return Response({'data':report.data}, status=status.HTTP_200_OK)


class ReportUpdate(APIView):
    """
    update or delete a report.
    """
    serializer_class = UserReportSerializer
    permission_classes = (IsAuthenticated,)
    renderer_classes = (ConduitJSONRenderer,)

    def post(self, request, id, format=None):
        try:
            report = UserReport.objects.get(id=id)
        except UserReport.DoesNotExist:
            return Response({'message': "Report not found."}, status=status.HTTP_404_NOT_FOUND)
        answer_data = request.data['answers']
        for answer in answer_data:
            UserReportAnswers.objects.update_or_create(
               category_id=answer['category_id'],
               question_id=answer['question_id'],
               patient_id=report.patient_id,
               report_id=report.id,
               defaults={
                   "answer": answer['answer'],
                   "duration": answer['duration']
                })

        Prescriptions.objects.update_or_create(
            doctor_id=request.user.id,
            patient_id=report.patient_id,
            report_id=report.id,
            defaults={
            "prescription": request.data['patient_prescription']['prescription']
        })
        return Response({'message': "Report has been updated."}, status=status.HTTP_200_OK)


class ReportDelete(APIView):
    """
    update or delete a report.
    """
    serializer_class = UserReportSerializer
    permission_classes = (IsAuthenticated,)
    renderer_classes = (ConduitJSONRenderer,)


    def delete(self, request, id, format=None):
        try:
            report = UserReport.objects.get(id=id)
        except UserReport.DoesNotExist:
            return Response({'message': "Report not found."}, status=status.HTTP_404_NOT_FOUND)
        UserReportAnswers.objects.filter(report_id=id).delete()
        Prescriptions.objects.filter(report_id=id).delete()
        report.delete()
        return Response({'message': "Report has been deleted."}, status=status.HTTP_200_OK)
