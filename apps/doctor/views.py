from rest_framework import status, generics, serializers, mixins, viewsets, permissions
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from apps.doctor.models import Doctors
from apps.patient.models import Patient
from apps.user.models import User
from apps.report.models import UserReport
from apps.appointment.models import Appointment
from apps.patient.serializers import (PatientListSerializer,)
from apps.user.serializers import (UserSerializer,)
from django.db.models import Q
from .renderers import UserJSONRenderer, PatientJSONRenderer
from .serializers import (
    RegistrationSerializer, LoginSerializer, CreatePatientSerializer, DoctorSerializer
)

import logging

logger = logging.getLogger(__name__)

class DoctorProfileChangeAPIView(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = RegistrationSerializer
    renderer_classes = (UserJSONRenderer,)

    def update(self, request, *args, **kwargs):
        user_data = request.data.get('user', {})
        serializer_data = request.data
        print(serializer_data['user']['email'])
        if(serializer_data['user']['email'] == request.user.email):
            serializer_data['user'].pop('email')
        if(serializer_data['user']['phone_number'] == request.user.phone_number):
            serializer_data['user'].pop('phone_number')

        # Here is that serialize, validate, save pattern we talked about
        # before.
        instance = Doctors.objects.filter(user_id=request.user.id).first()
        serializer = self.serializer_class(
            instance, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


class PatientViewSet(
                     mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    serializer_class = PatientListSerializer
    permission_classes = (IsAuthenticated,)
    renderer_classes = (PatientJSONRenderer,)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        user = self.request.user
        # logger.error(User.objects.filter(appointment_list_for_patient__doctor_id=user.id).query)
        # return Patient.objects.filter(Q(user__appointment_list_for_patient__doctor_id=user.id) | Q(user__added_by_user_type='doctor',user__added_by_user_id=user.id)).distinct()
        results = Patient.objects.filter(Q(appointment_list_for_patient__doctor_id=user.id) | Q(user__added_by_user_type='doctor',user__added_by_user_id=user.id)).order_by('-created_at').distinct()
        if self.request.GET.get('search'):
            search_term = self.request.GET.get('search')
            results = results.filter(Q(user__first_name__icontains=search_term) | Q(user__last_name__icontains=search_term) | Q(user__middle_name__icontains=search_term))

        return results

    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.serializer_class(
        page,
        context=serializer_context,
        many=True
        )

        return self.get_paginated_response(serializer.data)


class CreatePatientAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (UserJSONRenderer,)
    serializer_class = CreatePatientSerializer

    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        created = serializer.save()
        Patient.objects.filter(id=created.id).update(unique_patient_id='PNCRD00'+str(created.id))
        User.objects.filter(id=created.user_id).update(added_by_user_type='doctor',added_by_user_id=request.user.id)
        return Response(serializer.data, status=status.HTTP_200_OK)



class RegistrationAPIView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (UserJSONRenderer,)
    serializer_class = RegistrationSerializer

    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        created = serializer.save()
        Doctors.objects.filter(id=created.id).update(doctor_id='DNCRD00'+str(created.id),clinic_id='CNCRD00'+str(created.id))
        serializer.is_valid(raise_exception=True)
        serialized_data_copy=serializer.data
        serialized_data_copy['doctor_id'] = 'DNCRD00'+str(created.id)
        serialized_data_copy['clinic_id']='CNCRD00'+str(created.id)
        return Response(serialized_data_copy, status=status.HTTP_200_OK)


class LoginAPIView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (UserJSONRenderer,)
    serializer_class = LoginSerializer

    def post(self, request):
        user = request.data

        # Notice here that we do not call `serializer.save()` like we did for
        # the registration endpoint. This is because we don't actually have
        # anything to save. Instead, the `validate` method on our serializer
        # handles everything we need.
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class ProfileAPIView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (UserJSONRenderer,)
    serializer_class = DoctorSerializer

    def get_object(self):
        user = self.request.user
        doctor = Doctors.objects.get(user_id=user)
        return doctor


class DashboardAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (UserJSONRenderer,)

    def get(self, request, format=None):
        total_patients_appointed = Patient.objects.filter(appointment_list_for_patient__doctor_id=request.user.id).count()
        total_patients_created = Patient.objects.filter(user__added_by_user_id=request.user.id).count()
        total_patients = total_patients_appointed + total_patients_created
        total_appointments_clinic = Appointment.objects.filter(doctor=request.user.id).count()
        total_patient_record = UserReport.objects.filter(added_by_user_type="doctor",added_by_user_id=request.user.id).count()
        total_appointments_opd = 0
        content = {'total_patients': total_patients, 'total_appointments': total_appointments_clinic,'total_appointments_opd':total_appointments_opd,'total_patient_record':total_patient_record}
        return Response(content)
