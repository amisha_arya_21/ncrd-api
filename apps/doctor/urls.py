from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import (
    RegistrationAPIView, LoginAPIView, PatientViewSet, CreatePatientAPIView, ProfileAPIView,
    DashboardAPIView, DoctorProfileChangeAPIView
)

router = DefaultRouter(trailing_slash=False)
router.register(r'doctors/patients', PatientViewSet, basename='Doctor')

app_name="doctor"
urlpatterns = [

    url(r'^doctors/patients/create', CreatePatientAPIView.as_view()),
    url(r'^doctors/login/?$', LoginAPIView.as_view()),
    url(r'^doctors/dashboard/?$', DashboardAPIView.as_view()),
    url(r'^doctors/update/?$', DoctorProfileChangeAPIView.as_view()),
    url(r'^doctors/profile/?$', ProfileAPIView.as_view()),
    url(r'^doctors/?$', RegistrationAPIView.as_view()),
    url(r'^', include(router.urls)),
]
