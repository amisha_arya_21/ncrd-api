from django.db import models

# Create your models here.

class Doctors(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.OneToOneField(
        'user.User', on_delete=models.CASCADE, unique=True
    )
    doctor_id = models.CharField(max_length=255,blank=True, null=True, unique=True)
    clinic_id = models.CharField(max_length=100, blank=True, null=True, unique=True)
    department_id = models.BigIntegerField(blank=True, null=True)
    nmc_number = models.CharField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'doctors'
