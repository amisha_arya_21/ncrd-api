from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers, generics
from .models import Doctors
from apps.user.models import User
from apps.patient.models import Patient
from apps.user.serializers import (UserSerializer, )
# from apps.patient.serializers import UserListSerializer
import logging

logger = logging.getLogger(__name__)



class DoctorSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""

    user = UserSerializer()


    class Meta:
        model = Doctors
        fields = '__all__'
        read_only_fields = ('id',)



class RegistrationSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""

    user = UserSerializer()

    class Meta:
        model = Doctors
        fields = '__all__'
        read_only_fields = ('id','doctor_id')

    def create(self, validated_data):
        user_data = validated_data.pop('user');
        user = User.objects.create_user(**user_data)
        return Doctors.objects.create(user=user, **validated_data)

    def update(self, instance, validated_data):
        """Performs an update on a User."""
        user_data = validated_data.pop('user', {})
        # instance =
        for (key, value) in validated_data.items():
            # For the keys remaining in `validated_data`, we will set them on
            # the current `User` instance one at a time.
            setattr(instance, key, value)

        # Finally, after everything has been updated, we must explicitly save
        # the model. It's worth pointing out that `.set_password()` does not
        # save the model.
        instance.save()

        for (key, value) in user_data.items():
            if(key == "email" and value == instance.user.email):
                user_data.pop(key, {})
            elif(key == "phone_number" and value == instance.user.phone_number):
                print('there')
                user_data.pop(key, {})
            else:
                setattr(instance.user, key, value)

        instance.user.save()

        return instance

class CreatePatientSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new patient."""

    user = UserSerializer()


    class Meta:
        model = Patient
        fields = '__all__'
        read_only_fields = ('id',)


    def create(self, validated_data):
        user_data = validated_data.pop('user');
        user = User.objects.create_user(**user_data)
        patient = Patient.objects.create(user=user, **validated_data)
        return patient


class LoginSerializer(serializers.ModelSerializer):
    email = serializers.CharField(max_length=255, write_only=True)
    password = serializers.CharField(max_length=128, write_only=True)
    user = UserSerializer(read_only=True)
    doctor = DoctorSerializer(read_only=True)
    # nmc_number = serializers.CharField(source='doctor.nmc_number',read_only=True)

    class Meta:
        model = Doctors
        fields = '__all__'
        read_only_fields = ('id','nmc_number')

    def validate(self, data):
        # The `validate` method is where we make sure that the current
        # instance of `LoginSerializer` has "valid". In the case of logging a
        # user in, this means validating that they've provided an email
        # and password and that this combination matches one of the users in
        # our database.
        email = data.get('email', None)
        password = data.get('password', None)

        # As mentioned above, an email is required. Raise an exception if an
        # email is not provided.
        if email is None:
            raise serializers.ValidationError(
                'An email address is required to log in.'
            )

        # As mentioned above, a password is required. Raise an exception if a
        # password is not provided.
        if password is None:
            raise serializers.ValidationError(
                'A password is required to log in.'
            )

        # The `authenticate` method is provided by Django and handles checking
        # for a user that matches this email/password combination. Notice how
        # we pass `email` as the `username` value. Remember that, in our User
        # model, we set `USERNAME_FIELD` as `email`.
        user = authenticate(username=email, password=password)

        # If no user was found matching this email/password combination then
        # `authenticate` will return `None`. Raise an exception in this case.
        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password was not found.'
            )

        # Django provides a flag on our `User` model called `is_active`. The
        # purpose of this flag to tell us whether the user has been banned
        # or otherwise deactivated. This will almost never be the case, but
        # it is worth checking for. Raise an exception in this case.
        if not user.is_active:
            raise serializers.ValidationError(
                'This user has been deactivated.'
            )



        # The `validate` method should return a dictionary of validated data.
        # This is the data that is passed to the `create` and `update` methods
        # that we will see later on.

        # return Doctors.objects.get(user=user)
        doctor = Doctors.objects.get(user=user)
        # logger.error(doctor)
        return doctor
        # return {
        #     'user': user,
        #     'doctor': doctor
        # }
