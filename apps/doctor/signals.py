from apps.patient.models import Patient
from django.db.models.signals import post_save
from django.dispatch import receiver
import logging

logger = logging.getLogger(__name__)

@receiver(post_save, sender=Patient)
def create_unique_patient_id(sender, instance, created, **kwargs):
    logger.error('signal')
    # if created:
    #     patient = Patient.objects.get(id=instance.id)
    #     patient.unique_patient_id = 'PNCRD00'+str(instance.id)
    #     patient.save()
