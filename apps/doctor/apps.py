from django.apps import AppConfig


class DoctorConfig(AppConfig):
    name = 'apps.doctor'

    def ready(self):
            import apps.doctor.signals  # noqa
