from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers
from apps.patient.models import Patient
from apps.user.models import User
from .models import Questions
from apps.answers.models import UserReportAnswers
from apps.answers.serializers import (
    AnswerSerializer, CreateAnswerSerializer
)
# from apps.doctor.serializers import DoctorSerializer
# from apps.patient.serializers import PatientSerializer
import logging

logger = logging.getLogger(__name__)

class QuestionSerializer(serializers.ModelSerializer):
    answer = serializers.SerializerMethodField('get_answers')
    def get_answers(self, question):
        if self.context.get('report_id') is not None and self.context.get('report_id') != "":
            qs = UserReportAnswers.objects.filter(report_id=self.context['report_id'],question_id=question.id).first()
            serializer = CreateAnswerSerializer(instance=qs)
            return serializer.data
        else:
            qs = {
                "category_id": None,
                "patient_id": None,
                "report_id": None,
                "question_id": None,
                "answer": None,
                "duration": None
            }
            serializer = CreateAnswerSerializer(instance=qs)
            return serializer.data

    class Meta:
        model = Questions
        fields = ('id','category_id','content','answer')
        # fields = ('id','content')
        read_only_fields = ('id', 'content')
