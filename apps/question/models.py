from django.db import models

# Create your models here.
class Questions(models.Model):
    id = models.BigAutoField(primary_key=True)
    category = models.ForeignKey(
        'questionCategories.QuestionCategories', on_delete=models.CASCADE,
        related_name="categories_questions"
    )
    content = models.CharField(max_length=255)
    status = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'questions'
