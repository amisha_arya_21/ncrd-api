from rest_framework import status, mixins, permissions, generics
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from apps.common.renderers import ConduitJSONRenderer
from apps.prescription.models import Prescriptions
from apps.prescription.serializers import PrescriptionSerializer
from .renderers import QuestionJSONRenderer
from .models import Questions
from apps.report.models import UserReport
from apps.user.models import User
from .serializers import (
    QuestionSerializer,
)
import json

class ListQuestions(generics.ListAPIView):
    serializer_class = QuestionSerializer
    permission_classes = (IsAuthenticated,)
    renderer_classes = (ConduitJSONRenderer,)

    def get_queryset(self, category_id):
        return Questions.objects.filter(status=1,category_id=category_id)

    def list(self, request, category_id):
        queryset = self.get_queryset(category_id)
        report_id = request.GET['report_id']
        print("value "+report_id)
        if report_id is not None and report_id != "":
            try:
                report = UserReport.objects.get(id=report_id)
            except UserReport.DoesNotExist:
                raise ValidationError({'user': ['No report found.']})

        ids = queryset.values_list('id', flat=True)
        serializer = self.serializer_class(queryset,many=True, context={'request': request,'report_id':report_id})
        response_list = {}
        response_list['questions'] = serializer.data
        # response_list.append({'questions':serializer.data})
        prescription_data = {}
        if report_id is not None and report_id != "":
            prescription_data = Prescriptions.objects.filter(report_id=report_id,doctor_id=request.user.id).first()
            prescription_data = PrescriptionSerializer(prescription_data)
            response_list['patient_prescription'] = prescription_data.data
        else:
            response_list['patient_prescription'] = {
                "id": None,
                "prescription": None
            }

        return Response({'data':response_list},
                        status=status.HTTP_200_OK)


# class ListQuestions(generics.ListAPIView):
#     serializer_class = QuestionSerializer
#     permission_classes = (IsAuthenticated,)
#     renderer_classes = (ConduitJSONRenderer,)
#
#     def get_queryset(self):
#         return Questions.objects.filter(status=1)
#
#     def list(self, request, id):
#         queryset = self.get_queryset()
#         user_id = id
#         try:
#             user = User.objects.get(id=user_id)
#         except User.DoesNotExist:
#             raise ValidationError({'user': ['No user found with this id.']})
#         ids = queryset.values_list('id', flat=True)
#         serializer = self.serializer_class(queryset,many=True, context={'request': request,'user_id':user_id})
#         response_list = {}
#         response_list['questions'] = serializer.data
#         # response_list.append({'questions':serializer.data})
#         prescription_data = Prescriptions.objects.filter(patient_id=user_id,doctor=request.user).first()
#         prescription_data = PrescriptionSerializer(prescription_data)
#         response_list['patient_prescription'] = prescription_data.data
#         return Response({'data':response_list},
#                         status=status.HTTP_200_OK)
#
#
# class SaveQuestions(generics.CreateAPIView):
#     serializer_class = AnswerSerializer
#     permission_classes = (IsAuthenticated,)
#     renderer_classes = (ConduitJSONRenderer,)
#
#     def create(self, request, *args, **kwargs):
#         answers = request.data['question']
#         user_id = request.data['user_id']
#
#         for answer in answers:
#             data = {
#                 'user': user_id,
#                 'question':answer['id'],
#                 "answer": answer['answer'],
#                 "duration": answer['duration']
#             }
#             serializer = self.serializer_class(data=data)
#             serializer.is_valid(raise_exception=True)
#             Answers.objects.update_or_create(
#                question_id=answer['id'],
#                user_id=user_id,
#                defaults={
#                    "answer": answer['answer'],
#                    "duration": answer['duration']
#                 })
#             Prescriptions.objects.update_or_create(
#                doctor_id=request.user.id,
#                patient_id=user_id,
#                defaults={
#                    "prescription": request.data['patient_prescription']['prescription']
#                 })
#         return Response({'message': "Report is submitted."}, status=status.HTTP_200_OK)
