from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import (
    ListQuestions,
)
from django.urls import path

app_name="question"
urlpatterns = [
    # url(r'^questions/<int:id>?$', ListQuestions.as_view(), name="list-questions"),
    path('questions/<int:category_id>/', ListQuestions.as_view(), name="list-questions"),
    # url(r'^questions/save?$', SaveQuestions.as_view(), name="save-questions"),
]
