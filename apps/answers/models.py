from django.db import models

# Create your models here.
# class Answers(models.Model):
#     id = models.BigAutoField(primary_key=True)
#     user = models.ForeignKey(
#         'user.User', on_delete=models.CASCADE,
#         related_name='user'
#     )
#     question = models.ForeignKey(
#         'question.Questions', on_delete=models.CASCADE,
#         related_name='question_answers'
#     )
#     answer = models.CharField(max_length=10)
#     duration = models.CharField(max_length=150)
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#     deleted_at = models.DateTimeField(blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'user_answers'


class UserReportAnswers(models.Model):
    id = models.BigAutoField(primary_key=True)
    report = models.ForeignKey(
        'report.UserReport', on_delete=models.CASCADE,
        related_name='report_answer'
    )
    patient = models.ForeignKey(
        'patient.Patient', on_delete=models.CASCADE,
        related_name='report_patient'
    )
    question = models.ForeignKey(
        'question.Questions', on_delete=models.CASCADE,
        related_name='question_answers'
    )
    category = models.ForeignKey(
        'questionCategories.QuestionCategories', on_delete=models.CASCADE,
        related_name='answers_category'
    )
    answer = models.CharField(max_length=10, blank=True, null=True)
    duration = models.CharField(max_length=150, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'patient_report_answers'
