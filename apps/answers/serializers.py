from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers
from apps.patient.models import Patient
from apps.user.models import User
from .models import UserReportAnswers
# from apps.user.serializers import UserSerializer
# from apps.doctor.serializers import DoctorSerializer
# from apps.patient.serializers import PatientSerializer
import logging

logger = logging.getLogger(__name__)

class AnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserReportAnswers
        fields = ('question','answer','duration')
        read_only_fields = ('id',)


class CreateAnswerSerializer(serializers.Serializer):
    category_id = serializers.IntegerField(min_value=1)
    patient_id = serializers.IntegerField(min_value=1)
    report_id = serializers.IntegerField(min_value=1)
    question_id = serializers.IntegerField(min_value=1)
    answer = serializers.IntegerField()
    duration = serializers.CharField(max_length=150,allow_blank=True)


    def create(self, validated_data):
        return UserReportAnswers.objects.create(**validated_data)
