from django.apps import AppConfig


class QuestioncategoriesConfig(AppConfig):
    name = 'questionCategories'
