from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import (
    ListQuestionCategories,
)
from django.urls import path

app_name="questionCategories"
urlpatterns = [
    # url(r'^questions/<int:id>?$', ListQuestions.as_view(), name="list-questions"),
    path('categories/', ListQuestionCategories.as_view(), name="list-categories"),
    # url(r'^questions/save?$', SaveQuestions.as_view(), name="save-questions"),
]
