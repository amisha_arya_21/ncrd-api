from rest_framework import status, mixins, permissions, generics
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from apps.common.renderers import ConduitJSONRenderer
from .models import QuestionCategories
from apps.prescription.models import Prescriptions
from apps.prescription.serializers import PrescriptionSerializer
from apps.report.models import UserReport
from .serializers import (
    QuestionCategoriesSerializer,
)
import json

# Create your views here.
class ListQuestionCategories(generics.ListAPIView):
    serializer_class = QuestionCategoriesSerializer
    permission_classes = (IsAuthenticated,)
    renderer_classes = (ConduitJSONRenderer,)

    def get_queryset(self):
        return QuestionCategories.objects.filter(status=1).order_by('name')

    def list(self, request):
        queryset = self.get_queryset()
        # report_id = request.GET['report_id']
        report_id = request.GET.get('report_id', False)
        if report_id is not False and report_id != "":
            try:
                report = UserReport.objects.get(id=report_id)
            except UserReport.DoesNotExist:
                raise ValidationError({'user': ['No report found.']})

        serializer = self.serializer_class(queryset,many=True,context={'request': request,'report_id':report_id})
        response_list = {}
        response_list['questions'] = serializer.data
        prescription_data = {}
        if report_id is not None and report_id != "":
            prescription_data = Prescriptions.objects.filter(report_id=report_id,doctor_id=request.user.id).first()
            prescription_data = PrescriptionSerializer(prescription_data)
            response_list['patient_prescription'] = prescription_data.data
        else:
            response_list['patient_prescription'] = {
                "id": None,
                "prescription": None
            }

        return Response({'data':response_list},
                        status=status.HTTP_200_OK)
