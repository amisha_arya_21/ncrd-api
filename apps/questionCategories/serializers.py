from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers
from .models import QuestionCategories
from apps.question.serializers import QuestionSerializer
# from apps.patient.serializers import PatientSerializer
import logging

logger = logging.getLogger(__name__)

class QuestionCategoriesSerializer(serializers.ModelSerializer):
    # categories_questions = QuestionSerializer(many=True)
    categories_questions = serializers.SerializerMethodField()

    class Meta:
        model = QuestionCategories
        fields = ('id','name', 'categories_questions')
        read_only_fields = ('id', 'categories_questions')

    def get_categories_questions(self, instance):
        # Filter using the Car model instance and the CarType's related_name
        # (which in this case defaults to car_types_set)
        records = instance.categories_questions.filter(status=1)
        return QuestionSerializer(records, many=True,context=self.context).data
