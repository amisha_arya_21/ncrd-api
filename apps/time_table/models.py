from django.db import models

# Create your models here.
class DoctorsTimeTable(models.Model):
    id = models.BigAutoField(primary_key=True)
    # doctor_id = models.BigIntegerField()
    doctor = models.ForeignKey(
        'doctor.Doctors', on_delete=models.CASCADE,
        to_field='user_id',
        related_name='time_table_of_doctor'
    )
    day = models.IntegerField()
    from_time = models.TimeField(blank=True, null=True)
    to_time = models.TimeField(blank=True, null=True)
    from_time_2 = models.TimeField(blank=True, null=True)
    to_time_2 = models.TimeField(blank=True, null=True)
    is_closed = models.IntegerField(blank=True, null=True)
    is_available = models.IntegerField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        unique_together = ('day', 'doctor',)
        db_table = 'doctors_time_table'
