from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers, generics
from .models import DoctorsTimeTable
from apps.user.models import User
from apps.patient.models import Patient
from apps.user.serializers import UserSerializer
import datetime
# from apps.patient.serializers import UserListSerializer
import logging

logger = logging.getLogger(__name__)



class TimeTableSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""
    from_time = serializers.DateField(format="%H:%M %p",input_formats=["%H:%M","%s"])
    to_time = serializers.DateField(format="%H:%M %p",input_formats=["%H:%M","%s"])
    from_time_2 = serializers.DateField(format="%H:%M %p",input_formats=["%H:%M","%s"])
    to_time_2 = serializers.DateField(format="%H:%M %p",input_formats=["%H:%M","%s"])

    class Meta:
        model = DoctorsTimeTable
        fields = '__all__'
        read_only_fields = ('id',)

    def to_internal_value(self, data):
        if data.get('from_time', None) == '':
            data.from_time = "00:00:00"
        if data.get('from_time_2', None) == '':
            data.from_time_2 = "00:00:00"
        if data.get('to_time_2', None) == '':
            data.to_time_2 = "00:00:00"
        if data.get('to_time', None) == '':
            data.to_time = "00:00:00"
        return super(TimeTableSerializer, self).to_internal_value(data)
