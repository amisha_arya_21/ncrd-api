from rest_framework import status, generics, serializers, mixins, viewsets, permissions
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from apps.doctor.models import Doctors
from apps.patient.models import Patient
from apps.user.models import User
from apps.appointment.models import Appointment
from apps.patient.serializers import (PatientListSerializer,)
from apps.user.serializers import (UserSerializer,)
from django.db.models import Q
from .models import DoctorsTimeTable
from .serializers import (
    TimeTableSerializer
)
from .renderers import TimeTableJSONRenderer
from apps.common.renderers import ConduitJSONRenderer

import logging


class CreateTimeTableView(generics.CreateAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = TimeTableSerializer
    renderer_classes = (TimeTableJSONRenderer,)

    def create(self, request, *args, **kwargs):
        from_time = None
        from_time_2 = None
        to_time_2 = None
        to_time = None
        if 'from_time' in request.data:
            from_time = request.data['from_time']
        if 'from_time_2' in request.data:
            from_time_2 = request.data['from_time_2']
        if 'to_time_2' in request.data:
            to_time_2 = request.data['to_time_2']
        if 'to_time' in request.data:
            to_time = request.data['to_time']
        if from_time == '':
            from_time = None
        if from_time_2 == '':
            from_time_2 = None
        if to_time_2 == '':
            to_time_2 = None
        if to_time == '':
            to_time = None
        DoctorsTimeTable.objects.update_or_create(
           day=request.data['day'],
           doctor_id=request.user.id,
           defaults={
               "from_time": from_time,
               "from_time_2": from_time_2,
               "to_time": to_time,
               "to_time_2": to_time_2,
               "is_closed": request.data['is_closed'],
            })
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
        # try:
        #     response = super().create(request, args, kwargs)
        # except ValidationError as e:
        #     codes = e.get_codes()
        #     if pk_field in codes and codes[pk_field][0] == 'error':
        #         lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        #         self.kwargs[lookup_url_kwarg] = request.data[pk_field]
        #         return super().update(request, *args, **kwargs)
        #     else:
        #         raise e
        # return response

        # serializer.is_valid(raise_exception=True)
        # self.perform_create(serializer)
        # headers = self.get_success_headers(serializer.data)
        # return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

# class CreateTimeTableView(APIView):
#     permission_classes = IsAuthenticated,
#     serializer_class = TimeTableSerializer
#     renderer_classes = (TimeTableJSONRenderer,)
#
#     def post(self, request, *args, **kwargs):
#         serializer = self.get_serializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         self.perform_create(serializer)
#         headers = self.get_success_headers(serializer.data)
#         return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class UpdateTimeTableView(generics.UpdateAPIView):
    queryset = DoctorsTimeTable.objects.all()
    permission_classes = IsAuthenticated,
    serializer_class = TimeTableSerializer()
    renderer_classes = (TimeTableJSONRenderer,)
    lookup_field = 'id'


class TimeTableView(generics.ListAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = TimeTableSerializer
    renderer_classes = (TimeTableJSONRenderer,)

    def list(self,request):
        queryset = DoctorsTimeTable.objects.filter(doctor_id=request.user.id)
        serializer = TimeTableSerializer(queryset, many=True)
        return Response({'data': serializer.data}, status=status.HTTP_200_OK)
