from django.conf.urls import url, include
from django.urls import path
from .views import (
    TimeTableView, CreateTimeTableView, UpdateTimeTableView
)

app_name="time_table"
urlpatterns = [
    url(r'^time_table/?$', TimeTableView.as_view()),
    url(r'^time_table/create?$', CreateTimeTableView.as_view()),
    # url(r'^time_table/update/<id>?$', UpdateTimeTableView.as_view()),
    path('time_table/update/<id>', UpdateTimeTableView.as_view()),
]
