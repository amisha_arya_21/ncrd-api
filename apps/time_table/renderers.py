from apps.common.renderers import ConduitJSONRenderer


class TimeTableJSONRenderer(ConduitJSONRenderer):
    charset = 'utf-8'
    object_label = 'data'
    pagination_object_label = 'users'
    pagination_count_label = 'usersCount'

    def render(self, data, media_type=None, renderer_context=None):
        # If we recieve a `token` key as part of the response, it will by a
        # byte object. Byte objects don't serializer well, so we need to
        # decode it before rendering the User object.
        return super(TimeTableJSONRenderer, self).render(data)
