from django.db import models

# Create your models here.
class Prescriptions(models.Model):
    id = models.BigAutoField(primary_key=True)
    report = models.ForeignKey(
        'report.UserReport', on_delete=models.CASCADE,
        related_name='report_prescription'
    )
    doctor = models.ForeignKey(
        'doctor.Doctors', on_delete=models.CASCADE,
        to_field='id',
        related_name='prescription_doctor'
    )
    patient = models.ForeignKey(
        'patient.Patient', on_delete=models.CASCADE,
        to_field='id',
        related_name='prescription_patient'
    )
    prescription = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = False
        db_table = 'prescriptions'
