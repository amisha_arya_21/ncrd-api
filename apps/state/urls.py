from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import (
    ListState,
)
from django.urls import path

app_name="state"
urlpatterns = [
    # url(r'^questions/<int:id>?$', ListQuestions.as_view(), name="list-questions"),
    path('state/', ListState.as_view(), name="list-state"),
    # url(r'^questions/save?$', SaveQuestions.as_view(), name="save-questions"),
]
