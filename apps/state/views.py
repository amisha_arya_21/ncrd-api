from rest_framework import status, mixins, permissions, generics
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from apps.common.renderers import ConduitJSONRenderer
from .models import State
from .serializers import (
    StateSerializer,
)
import json


# Create your views here.

class ListState(generics.ListAPIView):
    serializer_class = StateSerializer
    permission_classes = (AllowAny,)
    renderer_classes = (ConduitJSONRenderer,)

    def get_queryset(self):
        return State.objects.filter(status=1)

    def list(self, request):
        queryset = self.get_queryset()
        serializer = self.serializer_class(queryset,many=True)
        return Response({'data':serializer.data},
                        status=status.HTTP_200_OK)
