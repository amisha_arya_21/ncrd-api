from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers
from .models import State
# from apps.doctor.serializers import DoctorSerializer
# from apps.patient.serializers import PatientSerializer
import logging

logger = logging.getLogger(__name__)

class StateSerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = ('id','name')
        read_only_fields = ('id', 'name')
