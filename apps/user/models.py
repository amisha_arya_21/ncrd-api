import jwt

from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin
)
from django.db import models
from apps.role.models import ModelHasRoles, Roles
from apps.state.models import State
from apps.city.models import City
from rest_framework.exceptions import ValidationError

class UserManager(BaseUserManager):
    """
    Django requires that custom users define their own Manager class. By
    inheriting from `BaseUserManager`, we get a lot of the same code used by
    Django to create a `User` for free.

    All we have to do is override the `create_user` function which we will use
    to create `User` objects.
    """

    def assign_role(self, user, role):
        return ModelHasRoles.objects.create(role=role,model_type='App\\User',model_id=user.id)

    def create_user(self, email, password=None, **extra_fields):
        """Create and return a `User` with an email and password."""
        if email is None:
            raise TypeError('Users must have an email address.')
        print(extra_fields)
        user = self.model(email=self.normalize_email(email), **extra_fields)
        try:
            role = Roles.objects.get(id=user.role_id)
        except Roles.DoesNotExist:
            raise ValidationError({'role': ['Invalid role.']})

        if password is not None:
            user.set_password(password)
        else:
            user.set_unusable_password()
        user.save()
        self.assign_role(user, role)
        return user

    def create_superuser(self, email, password, **extra_fields):
      """
      Create and return a `User` with superuser powers.

      Superuser powers means that this use is an admin that can do anything
      they want.
      """
      if password is None:
          raise TypeError('Superusers must have a password.')

      user = self.create_user(username, email, password, **extra_fields)
      # user.is_superuser = True
      # user.is_staff = True
      user.save()

      return user


class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    role_id = models.BigIntegerField(blank=True, null=True)
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=70, unique=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.IntegerField(default=1)
    phone_number = models.CharField(max_length=10)
    address = models.CharField(max_length=100, blank=True, null=True)
    # city_id = models.BigIntegerField(blank=True, null=True)
    # state_id = models.BigIntegerField(blank=True, null=True)
    state = models.ForeignKey(
        'state.State', on_delete=models.SET_NULL,blank=True, null=True,
        related_name='user_state'
    )
    city = models.ForeignKey(
        'city.City', on_delete=models.SET_NULL,blank=True, null=True,
        related_name='user_city'
    )
    citizenship = models.CharField(max_length=50, blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=10, blank=True, null=True)
    is_email_verified = models.IntegerField(default=0)
    email_verified_at = models.DateTimeField(blank=True, null=True)
    reset_token = models.CharField(max_length=255, blank=True, null=True)
    token_created_at = models.DateTimeField(blank=True, null=True)
    device_id = models.CharField(max_length=255, blank=True, null=True)
    device_type = models.CharField(max_length=20, blank=True, null=True)
    added_by_user_type = models.CharField(max_length=20, blank=True, null=True)
    added_by_user_id = models.CharField(max_length=20, blank=True, null=True)
    is_approved = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'

    # The `USERNAME_FIELD` property tells us which field we will use to log in.
    # In this case, we want that to be the email field.
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    # Tells Django that the UserManager class defined above should manage
    # objects of this type.
    objects = UserManager()

    def __str__(self):
        """
        Returns a string representation of this `User`.

        This string is used when a `User` is printed in the console.
        """
        return self.email

    @property
    def token(self):
        """
        Allows us to get a user's token by calling `user.token` instead of
        `user.generate_jwt_token().

        The `@property` decorator above makes this possible. `token` is called
        a "dynamic property".
        """
        return self._generate_jwt_token()

    def get_full_name(self):
      """
      This method is required by Django for things like handling emails.
      Typically, this would be the user's first and last name. Since we do
      not store the user's real name, we return their username instead.
      """
      return self.first_name + ' ' + self.middle_name + ' ' + self.last_name

    def get_short_name(self):
        """
        This method is required by Django for things like handling emails.
        Typically, this would be the user's first name. Since we do not store
        the user's real name, we return their username instead.
        """
        return self.first_name + ' ' + self.last_name

    def _generate_jwt_token(self):
        """
        Generates a JSON Web Token that stores this user's ID and has an expiry
        date set to 60 days into the future.
        """
        dt = datetime.now() + timedelta(days=60)

        token = jwt.encode({
            'id': self.pk,
            'exp': dt.utcfromtimestamp(dt.timestamp())
        }, settings.SECRET_KEY, algorithm='HS256')

        return token.decode('utf-8')
