from django.contrib.auth import authenticate
from rest_framework import serializers
from apps.state.serializers import StateSerializer
from apps.city.serializers import CitySerializer
from .models import User
from apps.state.models import State
from apps.city.models import City
import logging

logger = logging.getLogger(__name__)

class UserSerializer(serializers.ModelSerializer):
    """Handles serialization and deserialization of User objects."""

    # Passwords must be at least 8 characters, but no more than 128
    # characters. These values are the default provided by Django. We could
    # change them, but that would create extra work while introducing no real
    # benefit, so let's just stick with the defaults.
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True,
        required=False
    )
    # state = StateSerializer()
    # city = CitySerializer()

    # override the nested item field to PrimareKeyRelatedField on writes
    # def to_internal_value(self, data):
    #      self.fields['state'] = serializers.PrimaryKeyRelatedField(queryset=State.objects.all())
    #      self.fields['city'] = serializers.PrimaryKeyRelatedField(queryset=City.objects.all())
    #      return super(UserSerializer, self).to_internal_value(data)

    class Meta:
        model = User
        fields = ('id','role_id','password','token','email', 'first_name', 'last_name', 'middle_name', 'dob', 'phone_number', 'gender', 'address', 'city', 'state','citizenship')
        read_only_fields = ('token','id')


class ResetPasswordRequestSerializer(serializers.Serializer):
    email = serializers.EmailField()


class PasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
