from rest_framework import status, mixins, permissions, generics
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from .models import User
from django.contrib.auth.tokens import PasswordResetTokenGenerator
# from django.contrib.auth.tokens import default_token_generator
from .serializers import (
    ResetPasswordRequestSerializer, PasswordSerializer
)
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from datetime import datetime

from .renderers import UserJSONRenderer


class Logout(APIView):
    def get(self, request, format=None):
        # simply delete the token to force a login
        # request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)


class ResetPasswordRequestAPIView(APIView):
    """

    """
    serializer_class = ResetPasswordRequestSerializer
    permission_classes = (AllowAny,)
    renderer_classes = (UserJSONRenderer,)

    def post(self, request):
        data = request.data
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)
        email = data.get('email')
        user = User.objects.filter(email=email).first()
        active_user_found = False
        if user is None:
            raise ValidationError({'email': ['No user found with this email address.']})
        if user.is_active == 1:
            active_user_found = True

        if not active_user_found:
            raise ValidationError({'email': ['There is no active user associated with this e-mail address or the password can not be changed.']})

        if user.is_approved != 1:
            raise ValidationError({'email': ['Account has not been verified yet. Unable to change password.']})

        token = None
        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(user)
        user.reset_token = token
        user.token_created_at = datetime.now()
        user.save()
        url = settings.WEB_URL+"verify/"+token
        msg_plain = render_to_string('emails/reset_password.txt', {'url': url})
        msg_html = render_to_string('emails/reset_password.html', {'url': url})

        send_mail(
            'Reset Password',
            msg_plain,
            'ncrd@admin.com',
            [user.email],
            html_message=msg_html,
        )
        return Response({'message': "A password reset link has been sent to your email address."}, status=status.HTTP_200_OK)


class ChangePasswordView(APIView, mixins.UpdateModelMixin):
    """

    """
    serializer_class = PasswordSerializer
    permission_classes = (IsAuthenticated,)
    renderer_classes = (UserJSONRenderer,)

    def post(self, request):
        serializer = PasswordSerializer(data=request.data)

        if serializer.is_valid():
            user = request.user
            if not user.check_password(serializer.data.get('old_password')):
                raise ValidationError({'old_password': ['Wrong password.']})
                # return Response({'old_password': ['Wrong password.']},
                #                 status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            user.set_password(serializer.data.get('new_password'))
            user.save()
            return Response({'message': "Password has been updated successfully"})
            # return Response(user, status=status.HTTP_200_OK)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)
