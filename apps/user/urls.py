from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import (
    ResetPasswordRequestAPIView, ChangePasswordView, Logout
)

app_name="user"
urlpatterns = [
    url(r'^change-password/?$', ChangePasswordView.as_view(), name="change-password"),
    url(r'^reset-password/?$', ResetPasswordRequestAPIView.as_view(), name="reset-password-request"),
    url(r'^logout/', Logout.as_view()),
    # url(r'^user/?$', UserRetrieveUpdateAPIView.as_view()),
    # url(r'^users/login/?$', LoginAPIView.as_view()),
]
