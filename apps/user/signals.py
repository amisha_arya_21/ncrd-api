# from django.core.mail import EmailMultiAlternatives
# from django.dispatch import receiver
# from django.template.loader import render_to_string
# from django.urls import reverse
# from apps.emailTemplates.models import EmailTemplate
# from django_rest_passwordreset.signals import reset_password_token_created
# from django.conf import settings
# import logging
#
# logger = logging.getLogger(__name__)
#
# @receiver(reset_password_token_created)
# def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
#     """
#     Handles password reset tokens
#     When a token is created, an e-mail needs to be sent to the user
#     :param sender: View Class that sent the signal
#     :param instance: View Instance that sent the signal
#     :param reset_password_token: Token Model Object
#     :param args:
#     :param kwargs:
#     :return:
#     """
#     # send an e-mail to the user
#     logger.error(reverse('password_reset:reset-password-request'))
#     context = {
#         'current_user': reset_password_token.user,
#         # 'username': reset_password_token.user.username,
#         'email': reset_password_token.user.email,
#         'reset_password_url': "{}{}?token={}".format(settings.BASE_URL,reverse('password_reset:reset-password-request'), reset_password_token.key)
#     }
#
#     # render email text
#     template = EmailTemplate.objects.get(code='forgot-password')
#     email_html_message = render_to_string('user/user_reset_password.html', context)
#     email_plaintext_message = render_to_string('user/user_reset_password.txt', context)
#
#     msg = EmailMultiAlternatives(
#         # title:
#         "Password Reset for {title}".format(title="NCRD"),
#         # message:
#         email_plaintext_message,
#         # from:
#         "noreply@somehost.local",
#         # to:
#         [reset_password_token.user.email]
#     )
#     msg.attach_alternative(email_html_message, "text/html")
#     msg.send()
