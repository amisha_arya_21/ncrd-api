from django.conf.urls import url

from .views import (
    RegistrationAPIView, LoginAPIView
)

app_name="doctor"
urlpatterns = [
    # url(r'^user/?$', UserRetrieveUpdateAPIView.as_view()),
    url(r'^doctors/dashboard/?$', RegistrationAPIView.as_view()),
    url(r'^doctors/?$', RegistrationAPIView.as_view()),
    url(r'^doctors/login/?$', LoginAPIView.as_view()),
]
