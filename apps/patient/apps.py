from django.apps import AppConfig


class PtientConfig(AppConfig):
    name = 'apps.patient'
