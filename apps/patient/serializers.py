from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers
from .models import Patient
from apps.user.models import User
from apps.user.serializers import UserSerializer
from apps.appointment.serializers import AppointmentSerializer
from apps.state.serializers import StateSerializer
from apps.city.serializers import CitySerializer
from apps.appointment.models import Appointment
import logging
from datetime import datetime

logger = logging.getLogger(__name__)

class PatientSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""

    user = UserSerializer()
    appointments = AppointmentSerializer(many=True)

    class Meta:
        model = Patient
        fields = '__all__'
        read_only_fields = ('id',)


class UserListSerializer(serializers.ModelSerializer):
    """Handles serialization and deserialization of User objects."""

    # Passwords must be at least 8 characters, but no more than 128
    # characters. These values are the default provided by Django. We could
    # change them, but that would create extra work while introducing no real
    # benefit, so let's just stick with the defaults.
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True,
        required=False
    )
    state = StateSerializer()
    city = CitySerializer()

    class Meta:
        model = User
        fields = ('id','role_id','password','email', 'first_name', 'last_name', 'middle_name', 'dob', 'phone_number', 'gender', 'address','state', 'city', 'citizenship')
        read_only_fields = ('id',)


class PatientListSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""

    user = UserListSerializer()
    # appointment_list_for_patient = AppointmentSerializer(many=True)
    appointments = AppointmentSerializer(many=True)
    current_appointment = serializers.SerializerMethodField('get_current_appointment')

    def get_current_appointment(self, patient):
        # logger.error(self.context['request'].user.id)
        startdate = datetime.today()
        appointments = Appointment.objects.filter(doctor=self.context['request'].user.id, date__gte=startdate).first()
        if appointments:
            return AppointmentSerializer(instance=appointments).data
        # else:
            # return AppointmentSerializer(instance=appointments)


    class Meta:
        model = Patient
        fields = ('id','unique_patient_id','marital_status','user','current_appointment','appointments')
        read_only_fields = ('id',)
