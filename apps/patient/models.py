from django.db import models
from apps.appointment.models import Appointment
# Create your models here.

class Patient(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.OneToOneField(
        'user.User', on_delete=models.CASCADE, unique=True
    )
    unique_patient_id = models.CharField(max_length=255, blank=True, null=True, unique=True)
    marital_status = models.CharField(max_length=2, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'patients'

    @property
    def appointments(self):
        "Returns the current appointment of the patient."
        # return self.id
        return Appointment.objects.filter(patient=self.user_id)
