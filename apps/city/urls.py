from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import (
    ListCity,
)
from django.urls import path

app_name="city"
urlpatterns = [
    path('city/<int:state_id>/', ListCity.as_view(), name="list-city"),
]
