from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers
from .models import City
# from apps.doctor.serializers import DoctorSerializer
# from apps.patient.serializers import PatientSerializer
import logging

logger = logging.getLogger(__name__)

class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ('id','state_id','name')
        read_only_fields = ('id', 'state_id', 'name')
