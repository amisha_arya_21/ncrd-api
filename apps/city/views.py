from rest_framework import status, mixins, permissions, generics
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from apps.common.renderers import ConduitJSONRenderer
from .models import City
from .serializers import (
    CitySerializer,
)
import json


# Create your views here.

class ListCity(generics.ListAPIView):
    serializer_class = CitySerializer
    permission_classes = (AllowAny,)
    renderer_classes = (ConduitJSONRenderer,)

    def get_queryset(self, state_id):
        return City.objects.filter(status=1,state_id=state_id).order_by('name')

    def list(self, request, state_id):
        queryset = self.get_queryset(state_id)
        serializer = self.serializer_class(queryset,many=True)
        return Response({'data':serializer.data},
                        status=status.HTTP_200_OK)
