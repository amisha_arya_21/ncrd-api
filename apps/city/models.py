from django.db import models

# Create your models here.
class City(models.Model):
    id = models.BigAutoField(primary_key=True)
    state_id = models.BigIntegerField()
    name = models.CharField(max_length=100)
    status = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'city'
