from apps.common.renderers import ConduitJSONRenderer
import logging

logger = logging.getLogger(__name__)

class UserJSONRenderer(ConduitJSONRenderer):
    charset = 'utf-8'
    object_label = 'data'
    pagination_object_label = 'users'
    pagination_count_label = 'usersCount'

    def render(self, data, media_type=None, renderer_context=None):
        # If we recieve a `token` key as part of the response, it will by a
        # byte object. Byte objects don't serializer well, so we need to
        # decode it before rendering the User object.
        token = data.get('user.token', None)

        if token is not None and isinstance(token, bytes):
            # Also as mentioned above, we will decode `token` if it is of type
            # bytes.
            data['user']['token'] = token.decode('utf-8')

        return super(UserJSONRenderer, self).render(data)
