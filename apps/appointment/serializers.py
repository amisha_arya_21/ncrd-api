from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers
from apps.patient.models import Patient
from apps.user.models import User
from .models import Appointment
# from apps.user.serializers import UserSerializer
# from apps.doctor.serializers import DoctorSerializer
# from apps.patient.serializers import PatientSerializer
import logging

logger = logging.getLogger(__name__)

class AppointmentSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""
    from_time = serializers.DateTimeField(format='%I:%M %p')
    to_time = serializers.DateTimeField(format='%I:%M %p')

    class Meta:
        model = Appointment
        fields = '__all__'
        read_only_fields = ('id',)
