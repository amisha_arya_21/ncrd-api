from django.db import models
# from apps.doctor.models import Doctor
# from apps.patient.models import Patient
# Create your models here.

class Appointment(models.Model):
    id = models.BigAutoField(primary_key=True)
    doctor = models.ForeignKey(
        'doctor.Doctors', on_delete=models.CASCADE,
        to_field='user_id',
        related_name='appointment_list_for_doctor'
    )
    patient = models.ForeignKey(
        'patient.Patient', on_delete=models.CASCADE,
        to_field='user_id',
        related_name='appointment_list_for_patient'
    )
    date = models.DateField(blank=True, null=True)
    from_time = models.TimeField(blank=True, null=True)
    to_time = models.TimeField(blank=True, null=True)
    status = models.IntegerField(default=0,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'appointments'
