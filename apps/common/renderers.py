import json

from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse

class ConduitJSONRenderer(JSONRenderer):
    charset = 'utf-8'
    object_label = 'data'
    pagination_object_label = 'objects'
    pagination_object_count = 'count'

    def render(self, data, media_type=None, renderer_context=None):
        if not data:
          return json.dumps({
              "status": True,
              'data': {}
          })

        elif data.get('message', None) is not None:
            return json.dumps({
                "status": True,
                "data": data.get('message')
            })

        elif data.get('results', None) is not None:
            print(data)
            return json.dumps({
                "status": True,
                self.pagination_count_label: data['count'],
                "next": data['next'],
                self.pagination_object_label: data['results']
            })

        # If the view throws an error (such as the user can't be authenticated
        # or something similar), `data` will contain an `errors` key. We want
        # the default JSONRenderer to handle rendering errors, so we need to
        # check for this case.
        elif data.get('errors', None) is not None:
            error_data = data.get('errors', None)
            error_data_copy = {}
            for key, ele in error_data.items():
                if isinstance(ele,dict):
                # error_data_copy.pop(ele)
                    for k, v in ele.items():
                        error_data_copy[k] = v[0]
                else:
                    error_data_copy[key] = ele[0]
                    # error_data_copy[ele['code']] = 'hdvrhvb'
            return json.dumps({
                "status": False,
                "errors": error_data_copy
            })
            # return super(ConduitJSONRenderer, self).render(data)

        elif data.get('data', None) is not None:
            return json.dumps({
                "status": True,
                self.object_label: data.get(('data'))
            })

        else:
            return json.dumps({
                "status": True,
                self.object_label: data
            })
